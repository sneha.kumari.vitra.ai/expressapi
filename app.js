const express = require('express');
const https = require('https');
const app = express();

let detailArr = [];

app.get('/topStoriesApi', (req, resp) => {
    detailArr = [];
    hitExternalApi('top');
    resp.send(detailArr);
});

app.get('/bestStoriesApi', (req, resp) => {
    detailArr = [];
    hitExternalApi('best');
    resp.send(detailArr);
});

app.get('/newStoriesApi', (req, resp) => {
    detailArr = [];
    hitExternalApi('new');
    resp.send(detailArr);
});

function hitExternalApi(type) {
    var url = "";
    if(type == 'top') {
        url = 'https://hacker-news.firebaseio.com/v0/topstories.json';
    } else if(type == 'best') {
        url = 'https://hacker-news.firebaseio.com/v0/beststories.json';
    } else if(type == 'new') {
        url = 'https://hacker-news.firebaseio.com/v0/newstories.json';
    }
    console.log(url);
    https.get(url, (resp) => {
        let data = '';
        resp.on('data', (chunk) => {
            data += chunk;
        });

        resp.on('end', () => {
            return getDetails(data);
        });
    }).on("error", (err) => {
        console.log("Error: " + err.message);
    });
}

function getDetails(data) {
    var arr = JSON.parse(data);
    arr.forEach(element => { 
        var url = 'https://hacker-news.firebaseio.com/v0/item/' + element + '.json';
        https.get(url, (resp) => {
            let data = '';
            resp.on('data', (chunk) => {
                data += chunk;
            });

            resp.on('end', () => {
                getResponse(data);
            });
        }).on("error", (err) => {
            console.log("Error: " + err.message);
        });
    });
}

function getResponse(result) {
    detailArr.push(JSON.parse(result));
}

app.listen(3000, () => console.log('Listening to port 3000'));